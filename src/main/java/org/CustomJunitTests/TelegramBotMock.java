package org.CustomJunitTests;

import org.mrroot.TelegramBot;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.vk.VK;

import java.util.ArrayList;

public class TelegramBotMock extends TelegramBot{

    private ArrayList<String> testsMessage;
    private int currentDataSet = 0;

    public TelegramBotMock(VK _vkService)
    {
        super("", "", _vkService);
        testsMessage = new ArrayList<String>();
        testsMessage.add("test");
        testsMessage.add("test2");
        testsMessage.add("test3");
    }

    protected void sendMyMessage(SendMessage message)
    {
        if(!message.getText().equals(testsMessage.get(currentDataSet)))
        {
            System.out.println("Test failed " + message.getText() + " != " + testsMessage.get(currentDataSet) );
        }
        else
        {
            System.out.println("Test case " + (currentDataSet+1) + " ok");
        }

        currentDataSet++;
    }

}