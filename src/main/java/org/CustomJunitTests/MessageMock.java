package org.CustomJunitTests;


import org.telegram.telegrambots.api.objects.Message;

public class MessageMock extends Message {
    public boolean hasText()
    {
        return true;
    }

    public Long getChatId()
    {
        return new Long(0);
    }
}