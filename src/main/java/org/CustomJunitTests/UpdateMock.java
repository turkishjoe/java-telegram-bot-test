package org.CustomJunitTests;


import org.telegram.telegrambots.api.objects.Update;
import org.vk.VK;

public class UpdateMock extends Update {
    private MessageMock messageMock;

    public UpdateMock()
    {
        messageMock = new MessageMock();
    }

    public MessageMock getMessage() {
        return messageMock;
    }

    public boolean hasMessage()
    {
        return true;
    }
}