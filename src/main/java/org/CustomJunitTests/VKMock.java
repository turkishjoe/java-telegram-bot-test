package org.CustomJunitTests;


import org.json.JSONArray;
import org.json.JSONObject;
import org.vk.Post;
import org.vk.VK;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

public class VKMock extends VK {


    public VKMock() {
        super("");
    }

    protected String getResponse()
    {
        return "{\"response\":{\"count\":11605,\"items\":[{\"id\":150393,\"from_id\":-59153742,\"owner_id\":-59153742,\"date\":1511374200,\"marked_as_ads\":0,\"post_type\":\"post\",\"text\":\"test\",\"post_source\":{\"type\":\"api\",\"platform\":\"iphone\"},\"comments\":{\"count\":1,\"groups_can_post\":true,\"can_post\":1},\"likes\":{\"count\":0,\"user_likes\":0,\"can_like\":1,\"can_publish\":1},\"reposts\":{\"count\":0,\"user_reposted\":0},\"views\":{\"count\":1162}},{\"id\":150388,\"from_id\":-59153742,\"owner_id\":-59153742,\"date\":1511370600,\"marked_as_ads\":0,\"post_type\":\"post\",\"text\":\"test2\",\"post_source\":{\"type\":\"api\",\"platform\":\"iphone\"},\"comments\":{\"count\":1,\"groups_can_post\":true,\"can_post\":1},\"likes\":{\"count\":0,\"user_likes\":0,\"can_like\":1,\"can_publish\":1},\"reposts\":{\"count\":0,\"user_reposted\":0},\"views\":{\"count\":1251}},{\"id\":150385,\"from_id\":-59153742,\"owner_id\":-59153742,\"date\":1511368800,\"marked_as_ads\":0,\"post_type\":\"post\",\"text\":\"test3\",\"post_source\":{\"type\":\"api\",\"platform\":\"iphone\"},\"comments\":{\"count\":1,\"groups_can_post\":true,\"can_post\":1},\"likes\":{\"count\":0,\"user_likes\":0,\"can_like\":1,\"can_publish\":1},\"reposts\":{\"count\":0,\"user_reposted\":0},\"views\":{\"count\":1271}}]}}";
    }
}