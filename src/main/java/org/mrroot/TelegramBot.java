package org.mrroot;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.generics.LongPollingBot;
import org.vk.Post;
import org.vk.VK;

import java.util.ArrayList;

public class TelegramBot extends TelegramLongPollingBot{

    /**
     *
     */
    private String botName = null;

    /**
     *
     */
    private String apiKey = null;

    protected ArrayList<Integer> alreadySendedPosts ;

    protected VK vkService = null;

    public TelegramBot(String _apiKey, String _botName, VK _vkService)
    {
        super();
        vkService = _vkService;
        apiKey = _apiKey;
        botName = _botName;
        alreadySendedPosts = new ArrayList<Integer>();
    }

    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            ArrayList<Post> posts = vkService.getPosts();
            for(int i = 0; i < posts.size(); i++)
            {
                Post post = posts.get(i);
                if(!alreadySendedPosts.contains(post.getId())) {
                    SendMessage message = new SendMessage() // Create a SendMessage object with mandatory fields
                            .setChatId(update.getMessage().getChatId())
                            .setText(post.getContent());

                    sendMyMessage(message);
                    alreadySendedPosts.add(post.getId());
                }
            }
        }
    }

    public String getBotUsername() {
        // TODO
        return botName;
    }

    @Override
    public String getBotToken() {
        // TODO
        return apiKey;
    }

    protected void sendMyMessage(SendMessage message)
    {
        try {
            execute(message); // Call method to send the message
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}