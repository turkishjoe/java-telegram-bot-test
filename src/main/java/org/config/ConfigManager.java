package org.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

/**
 * TODO
 * Created by prog12 on 02.09.17.
 */
public class ConfigManager {

    public HashMap<String, Properties> savedProperties;

    public ConfigManager()
    {
        savedProperties = new HashMap<String, Properties>();
    }

    public Properties getProperties(String configName) throws IOException
    {
        if(!savedProperties.containsKey(configName)) {
            FileInputStream input = null;
            Properties prop = new Properties();
            boolean isError = false;
            try {
                input = new FileInputStream("config/" + configName + ".ini");
                prop.load(input);
                savedProperties.put(configName, prop);
            } catch (IOException io) {
                isError = true;
            } finally {
                if (input != null) {
                   input.close();
                }

                if(isError)
                {
                    throw new IOException("Config " + configName + " not found");
                }
            }
        }

        return savedProperties.get(configName);
    }

}
