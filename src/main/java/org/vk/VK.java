package org.vk;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VK {
    private String serviceToken;

    public VK(String _serviceToken)
    {
        serviceToken = _serviceToken;
    }

    public ArrayList<Post> getPosts()
    {
        ArrayList<Post> posts = new ArrayList<Post>();

        String response = getResponse();
        JSONObject obj = new JSONObject(response);
        JSONArray arr = obj.getJSONObject("response").getJSONArray("items");

        for (int i = 0 ; i < arr.length(); i++)
        {
            JSONObject row = arr.getJSONObject(i);
            Post post = new Post();
            post.setId((Integer)row.get("id"));
            post.setContent((String) row.get("text"));
            posts.add(post);
        }

        return posts;
    }

    protected String getResponse()
    {
        return ClientBuilder.newBuilder().newClient().target("https://api.vk.com/method/wall.get?domain=overheareltech&access_token=" + serviceToken + "&v=5.69").request().get().readEntity(String.class);
    }
}