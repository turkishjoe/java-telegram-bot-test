package main.java;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.config.ConfigManager;
import org.mrroot.TelegramBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.vk.Post;
import org.vk.VK;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.*;

public class Main
{
    public static void main(String[] args) throws IOException {
        ApiContextInitializer.init();
        ConfigManager configManager = new ConfigManager();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        Properties prop = configManager.getProperties("telegram");

        if(prop != null) {
            VK vk = new VK(prop.getProperty("vk_service_key"));
            try {
                botsApi.registerBot(new TelegramBot(prop.getProperty("telegram_bot_api_key"), prop.getProperty("telegram_bot_name"), vk));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}