import org.CustomJunitTests.TelegramBotMock;
import org.CustomJunitTests.UpdateMock;
import org.CustomJunitTests.VKMock;
import org.config.ConfigManager;
import org.mrroot.TelegramBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.vk.VK;

import java.io.IOException;
import java.util.Properties;

public class CustomJunit
{
    public static void main(String[] args) throws IOException {
        VKMock vk = new VKMock();
        TelegramBotMock tg = new TelegramBotMock(vk);
        tg.onUpdateReceived(new UpdateMock());
    }
}